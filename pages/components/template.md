---
name: Component name
figma: Link to figma component in Pajamas UI Kit
docs: upcoming | in-progress | complete
a11y: upcoming | in-progress | complete
gitlab_ui: Relative link to that component‘s /code tab. For example, /component/popover/code.
vueComponents:
  - Related vue component name

<!--
  RELATED VUE COMPONENTS should be added when available
  Remove this section if there are no related vue components at this time. Please list them in alphabetical order.
-->

related:
  - Related component or pattern name

<!--
  RELATED PATTERNS should be similar in usage/type of pattern
  e.g. tooltips, popover, and modals are all similar constructs used for different purposes
  Remove this section if there are no related patterns at this time. Please list them in alphabetical order.

  EXAMPLE RELATED PATTERNS, link to pages that are not components using 'directory/pattern-name'
  related:
    - /layout/grid
    - badge
-->
---

Component overview. A brief explanation of what the component is.

## Usage

A summary of how the component should be used.

<!--
  DO NOT add static images to any page at this time.
-->

### Dos and Dont’s

<!--
  EXAMPLE TABLE, optionally, this can be used to highlight Do's & Don'ts
  Do is a guideline that should always be followed. On the other hand, you need a really unusual use case for breaking a Do guideline
  The Do's & Don'ts should be correlated
  e.g. Use chevrons between breadcrumb items vs. Use slashes or other characters to separate breadcrumb items.
-->

| Component type | Purpose |
| --- | --- |
| Primary button | The primary call to action on the page |
| Secondary button | Secondary actions on the page |

### When to use INSERT NAME OF COMPONENT HERE

<!--
  WHEN TO USE, optionally, highlight specific rule sets for when to use a component.
  e.g. Use tables when:
  - Users need to review, enter, or edit uniform sets of data or options
  - Displaying structured content, where each entry has the same attributes
-->

### When not to use INSERT NAME OF COMPONENT HERE

<!--
  WHEN NOT TO USE, optionally, highlight specific rule sets for when not to use a component
  e.g. Do not use tables to:
  - Display a list of continuous, vertical indexes of text or images. Use Lists instead
  - For hierarchical structures. Use the Tree view instead.
-->

### Sub section

<!--
  SUB SECTIONS, use these to highlight component specific rules. You can add as many sub-sections as needed, use your best judgement
  e.g. truncation rules
-->

A summary of a specific usage guideline.

Todo: Add live component block with code example

## Demo

<!--
  DEMO, keep this section for all patterns, the code block demo will be added at a later date
-->

Todo: Add live component block with code example

## Design specifications

<!--
  DESIGN SPECIFICATIONS, add a link here to the component in the Pajamas UI Kit. In most cases this will be a link the component’s “Variants” frame. You can find this link by clicking on the “Variants” frame and then copying the link from the “Share” option.
  *** Follow the “Figma component” issue template in Pajamas to create the component.
-->

Color, spacing, dimension, and layout specific information pertaining to this component can be viewed using the following link:

[View design in Pajamas UI Kit →](/)

## Resources

- [A related resource used when writing this documentation](/)
